package com.progressoft.mpclear.cache.demo.mappers;

import com.progressoft.mpclear.cache.demo.model.Customer;
import com.progressoft.mpclear.cache.demo.persistence.entity.CustomerEntity;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface CustomerMapper {
    Customer toModel(CustomerEntity customerEntity);

    CustomerEntity toEntity(Customer customer);
}
