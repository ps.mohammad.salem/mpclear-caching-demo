package com.progressoft.mpclear.cache.demo.persistence.dao;

import com.progressoft.mpclear.cache.demo.mappers.AccountMapper;
import com.progressoft.mpclear.cache.demo.mappers.CustomerMapper;
import com.progressoft.mpclear.cache.demo.model.Customer;
import com.progressoft.mpclear.cache.demo.persistence.entity.AccountEntity;
import com.progressoft.mpclear.cache.demo.persistence.entity.CustomerEntity;
import com.progressoft.mpclear.cache.demo.persistence.repository.JpaAccountRepository;
import com.progressoft.mpclear.cache.demo.persistence.repository.JpaCustomerRepository;
import com.progressoft.mpclear.cache.demo.requests.AccountRequest;
import lombok.AllArgsConstructor;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.cache.annotation.Caching;
import org.springframework.stereotype.Component;

import java.util.List;

import static java.util.stream.Collectors.toList;

@Component
@AllArgsConstructor
public class CustomerDaoImpl implements CustomerDao {
    private final AccountMapper accountMapper;
    private final CustomerMapper customerMapper;
    private final JpaCustomerRepository jpaCustomerRepository;
    private final JpaAccountRepository jpaAccountRepository;

    @Override
    public void save(Customer customer) {
        jpaCustomerRepository.save(customerMapper.toEntity(customer));
        jpaAccountRepository.saveAll(customer.getAccounts().stream()
                .map(accountMapper::toEntity)
                .collect(toList()));
    }

    @Override
    @Caching(evict = {
            @CacheEvict(value = "customerByAlias", key = "#request.oldAlias"),
            @CacheEvict(value = "customerByMobile", key = "#request.oldMobile")
    })
    public void update(AccountRequest request) {
        if (request.getId().equals("2"))
            throw new RuntimeException("x");
        jpaAccountRepository.save(accountMapper.toEntity(accountMapper.toModel(request)));
    }

    @Override
    @Cacheable(value = "customerByAlias", unless = "#result == null")
    public Customer getCustomerByAlias(String alias) {
        return getCustomer(jpaAccountRepository.getByAlias(alias).getCustomerId());
    }

    @Override
    @Cacheable(value = "customerByMobile", unless = "#result == null")
    public Customer getCustomerByMobile(String mobile) {
        return getCustomer(jpaAccountRepository.getByMobile(mobile).getCustomerId());
    }

    private Customer getCustomer(String customerId) {
        CustomerEntity customerEntity = jpaCustomerRepository.getOne(customerId);
        List<AccountEntity> customerAccounts = jpaAccountRepository.findAllByCustomerId(customerEntity.getId());
        return toCustomer(customerEntity, customerAccounts);
    }

    private Customer toCustomer(CustomerEntity customerEntity, List<AccountEntity> customerAccounts) {
        Customer customer = customerMapper.toModel(customerEntity);
        customer.setAccounts(customerAccounts.stream().map(accountMapper::toModel).collect(toList()));
        return customer;
    }
}
