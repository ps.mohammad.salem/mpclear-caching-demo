package com.progressoft.mpclear.cache.demo.aspect;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.stereotype.Component;
import org.springframework.util.StopWatch;

import static java.util.stream.Collectors.joining;
import static java.util.stream.IntStream.range;

@Aspect
@Component
public class LoggingAspect {
    private static final ThreadLocal<Integer> level = ThreadLocal.withInitial(() -> 0);
    private static final Logger LOGGER = LogManager.getLogger(LoggingAspect.class);

    //AOP expression for which methods shall be intercepted
    @Around("execution(* com.progressoft..*(..)))")
    public Object profileAllMethods(ProceedingJoinPoint proceedingJoinPoint) throws Throwable {
        MethodSignature methodSignature = (MethodSignature) proceedingJoinPoint.getSignature();

        //Get intercepted method details
        String className = methodSignature.getDeclaringType().getSimpleName();
        String methodName = methodSignature.getName();

        final StopWatch stopWatch = new StopWatch();
        try {
            level.set(level.get() + 1);
            //Measure method execution time
            stopWatch.start();
            Object result = proceedingJoinPoint.proceed();
            stopWatch.stop();
            LOGGER.info(indent() + className + "." + methodName + " :: " + stopWatch.getTotalTimeMillis() + " ms");
            return result;
        } finally {
            level.set(level.get() - 1);
        }
        //Log method execution time
    }

    private String indent() {
        return range(0, level.get()).mapToObj(i -> "===").collect(joining()) + "> ";
    }
}