package com.progressoft.mpclear.cache.demo.requests;

import com.sun.istack.NotNull;
import lombok.Data;

import java.math.BigDecimal;

@Data
public class PaymentRequest {
    @NotNull
    private String id;
    private String debtorAlias;
    private String debtorMobile;
    private String creditorAlias;
    private String creditorMobile;
    @NotNull
    private String type;
    @NotNull
    private BigDecimal amount;
}
