package com.progressoft.mpclear.cache.demo.persistence.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.Id;
import java.io.Serializable;

@Data
@Entity
@NoArgsConstructor
@AllArgsConstructor
public class AccountEntity implements Serializable {
    @Id
    private String id;
    private String customerId;
    private String alias;
    private String mobile;
}
