package com.progressoft.mpclear.cache.demo.persistence.repository;

import com.progressoft.mpclear.cache.demo.persistence.entity.AccountEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface JpaAccountRepository extends JpaRepository<AccountEntity, String> {
    AccountEntity getByAlias(String alias);

    AccountEntity getByMobile(String mobile);

    List<AccountEntity> findAllByCustomerId(String customerId);
}
