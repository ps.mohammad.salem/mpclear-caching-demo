package com.progressoft.mpclear.cache.demo.controllers;

import com.progressoft.mpclear.cache.demo.requests.PaymentRequest;
import com.progressoft.mpclear.cache.demo.services.PaymentService;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController
@AllArgsConstructor
public class PaymentController {
    private final PaymentService paymentService;

    @PostMapping("/payment")
    public void process(@Valid @RequestBody PaymentRequest request) {
        paymentService.process(request);
    }
}
