package com.progressoft.mpclear.cache.demo.jobs;

import com.progressoft.mpclear.cache.demo.model.Session;
import com.progressoft.mpclear.cache.demo.persistence.dao.SessionDao;
import lombok.AllArgsConstructor;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import javax.transaction.Transactional;
import java.time.LocalDateTime;
import java.util.UUID;

@Component
@AllArgsConstructor
public class SessionJobs {
    private final SessionDao sessionDao;

    @Transactional
    @Scheduled(fixedRateString = "1000")
    public void open() {
        LocalDateTime now = LocalDateTime.now();
        sessionDao.open(Session.builder()
                .id(UUID.randomUUID().toString())
                .sessionStartTime(now)
                .sessionEndTime(now.plusMinutes(1))
                .messageType(now.getSecond() % 2 == 0 ? "Credit" : "Debit")
                .build());
    }

    @Transactional
    @Scheduled(fixedRateString = "5000")
    public void close() {
        LocalDateTime now = LocalDateTime.now();
        sessionDao.list().stream()
                .filter(s -> s.getSessionEndTime().isAfter(now))
                .map(Session::getId)
                .forEach(sessionDao::close);
    }
}
