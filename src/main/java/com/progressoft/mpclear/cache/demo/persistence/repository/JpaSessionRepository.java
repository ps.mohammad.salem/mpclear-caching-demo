package com.progressoft.mpclear.cache.demo.persistence.repository;

import com.progressoft.mpclear.cache.demo.persistence.entity.SessionEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface JpaSessionRepository extends JpaRepository<SessionEntity, String> {
    List<SessionEntity> findAllByState(String state);
}
