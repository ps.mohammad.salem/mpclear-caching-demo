package com.progressoft.mpclear.cache.demo.mappers;

import com.progressoft.mpclear.cache.demo.model.Payment;
import com.progressoft.mpclear.cache.demo.persistence.entity.PaymentEntity;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface PaymentMapper {
    Payment toModel(PaymentEntity paymentEntity);

    PaymentEntity toEntity(Payment payment);
}
