package com.progressoft.mpclear.cache.demo.mappers;

import com.progressoft.mpclear.cache.demo.model.Account;
import com.progressoft.mpclear.cache.demo.requests.AccountRequest;
import com.progressoft.mpclear.cache.demo.persistence.entity.AccountEntity;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface AccountMapper {
    Account toModel(AccountEntity accountEntity);

    Account toModel(AccountRequest request);

    AccountEntity toEntity(Account account);
}
