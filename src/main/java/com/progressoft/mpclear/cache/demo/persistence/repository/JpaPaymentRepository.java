package com.progressoft.mpclear.cache.demo.persistence.repository;

import com.progressoft.mpclear.cache.demo.persistence.entity.PaymentEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface JpaPaymentRepository extends JpaRepository<PaymentEntity, String> {
}
