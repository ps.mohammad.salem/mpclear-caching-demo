package com.progressoft.mpclear.cache.demo.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.time.LocalDateTime;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Session implements Serializable {
    private String id;
    private LocalDateTime sessionStartTime;
    private LocalDateTime sessionEndTime;
    private String messageType;
}
