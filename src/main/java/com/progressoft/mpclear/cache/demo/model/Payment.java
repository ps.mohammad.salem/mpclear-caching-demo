package com.progressoft.mpclear.cache.demo.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.math.BigDecimal;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Payment implements Serializable {
    private String id;
    private String debtorId;
    private String debtorName;
    private String debtorMobile;
    private String creditorId;
    private String creditorName;
    private String creditorMobile;
    private String type;
    private String session;
    private BigDecimal amount;
}
