package com.progressoft.mpclear.cache.demo.mappers;

import com.progressoft.mpclear.cache.demo.model.Session;
import com.progressoft.mpclear.cache.demo.persistence.entity.SessionEntity;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface SessionMapper {
    Session toModel(SessionEntity sessionEntity);

    SessionEntity toEntity(Session session);
}
