package com.progressoft.mpclear.cache.demo.requests;

import com.sun.istack.NotNull;
import lombok.Data;

@Data
public class AccountRequest {
    @NotNull
    private String id;
    @NotNull
    private String customerId;
    @NotNull
    private String oldAlias;
    @NotNull
    private String alias;
    @NotNull
    private String oldMobile;
    @NotNull
    private String mobile;
}
