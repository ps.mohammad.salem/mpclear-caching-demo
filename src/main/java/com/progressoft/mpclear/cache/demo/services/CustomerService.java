package com.progressoft.mpclear.cache.demo.services;

import com.progressoft.mpclear.cache.demo.requests.AccountRequest;
import com.progressoft.mpclear.cache.demo.mappers.AccountMapper;
import com.progressoft.mpclear.cache.demo.model.Customer;
import com.progressoft.mpclear.cache.demo.persistence.dao.CustomerDao;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

@Service
@AllArgsConstructor
public class CustomerService {
    private final CustomerDao dao;
    private final AccountMapper mapper;

    @Transactional
    public void update(AccountRequest request) {
        dao.update(request);
    }

    public Customer getByMobile(String mobile) {
        return dao.getCustomerByMobile(mobile);
    }

    public Customer getByAlias(String alias) {
        return dao.getCustomerByAlias(alias);
    }
}
