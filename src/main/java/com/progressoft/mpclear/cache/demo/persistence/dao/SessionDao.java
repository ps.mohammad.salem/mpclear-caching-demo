package com.progressoft.mpclear.cache.demo.persistence.dao;

import com.progressoft.mpclear.cache.demo.model.Session;

import java.util.List;

public interface SessionDao {
    void open(Session session);

    void close(String id);

    List<Session> list();
}
