package com.progressoft.mpclear.cache.demo.services;

import com.progressoft.mpclear.cache.demo.requests.PaymentRequest;
import com.progressoft.mpclear.cache.demo.model.Account;
import com.progressoft.mpclear.cache.demo.model.Customer;
import com.progressoft.mpclear.cache.demo.model.Payment;
import com.progressoft.mpclear.cache.demo.persistence.dao.CustomerDao;
import com.progressoft.mpclear.cache.demo.persistence.dao.PaymentDao;
import com.progressoft.mpclear.cache.demo.model.Session;
import com.progressoft.mpclear.cache.demo.persistence.dao.SessionDao;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

import static org.apache.commons.lang3.StringUtils.isNotBlank;

@Service
@AllArgsConstructor
public class PaymentService {
    private final CustomerDao customerDao;
    private final SessionDao sessionDao;
    private final PaymentDao paymentDao;

    @Transactional
    public void process(PaymentRequest request) {
        CustomerAndAccount creditor = getCustomerAndAccount(request.getCreditorAlias(), request.getCreditorMobile());
        CustomerAndAccount debtor = getCustomerAndAccount(request.getDebtorAlias(), request.getDebtorMobile());
        paymentDao.persist(Payment.builder()
                .id(request.getId())
                .debtorId(debtor.customer.getId())
                .debtorName(debtor.customer.getName())
                .debtorMobile(debtor.account.getMobile())
                .creditorId(creditor.customer.getId())
                .creditorName(creditor.customer.getName())
                .creditorMobile(creditor.account.getMobile())
                .type(request.getType())
                .amount(request.getAmount())
                .session(sessionDao.list().stream()
                        .filter(s -> s.getMessageType().equals(request.getType()))
                        .map(Session::getId)
                        .findFirst()
                        .orElse(null))
                .build());
    }

    private CustomerAndAccount getCustomerAndAccount(String alias, String mobile) {
        Customer customer = isNotBlank(alias) ?
                customerDao.getCustomerByAlias(alias) :
                customerDao.getCustomerByMobile(mobile);
        Account account = isNotBlank(alias) ?
                customer.getAccounts().stream().filter(a -> alias.equals(a.getAlias())).findFirst().orElse(null) :
                customer.getAccounts().stream().filter(a -> mobile.equals(a.getMobile())).findFirst().orElse(null);
        return new CustomerAndAccount(customer, account);
    }


    @AllArgsConstructor
    static class CustomerAndAccount {
        private final Customer customer;
        private final Account account;
    }
}
