package com.progressoft.mpclear.cache.demo.persistence.dao;

import com.progressoft.mpclear.cache.demo.model.Payment;

public interface PaymentDao {
    void persist(Payment payment);
}
