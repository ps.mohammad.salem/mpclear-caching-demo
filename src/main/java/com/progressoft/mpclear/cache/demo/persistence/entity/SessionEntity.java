package com.progressoft.mpclear.cache.demo.persistence.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.Id;
import java.io.Serializable;
import java.time.LocalDateTime;

@Data
@Entity
@NoArgsConstructor
@AllArgsConstructor
public class SessionEntity implements Serializable {
    @Id
    private String id;
    private LocalDateTime sessionStartTime;
    private LocalDateTime sessionEndTime;
    private String messageType;
    private String state;
}
