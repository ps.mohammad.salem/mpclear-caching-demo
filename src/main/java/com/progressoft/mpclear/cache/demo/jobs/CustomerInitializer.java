package com.progressoft.mpclear.cache.demo.jobs;

import com.github.javafaker.Faker;
import com.progressoft.mpclear.cache.demo.model.Account;
import com.progressoft.mpclear.cache.demo.model.Customer;
import com.progressoft.mpclear.cache.demo.persistence.dao.CustomerDao;
import lombok.AllArgsConstructor;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import javax.transaction.Transactional;
import java.util.UUID;
import java.util.stream.IntStream;

import static java.util.Collections.singletonList;

@Component
@AllArgsConstructor
public class CustomerInitializer implements CommandLineRunner {
    private final CustomerDao customerDao;

    @Transactional
    @Override
    public void run(String... args) {
        try {
            Faker faker = new Faker();
            IntStream.range(0, 1000).boxed().map(Object::toString).forEach(id ->
                    customerDao.save(Customer.builder()
                            .id(id)
                            .name(faker.name().name())
                            .accounts(singletonList(Account.builder()
                                    .id(id)
                                    .customerId(id)
                                    .alias(id)
                                    .mobile(id)
                                    .build()))
                            .build()));
        } catch (Exception e) {
            System.out.println("customers are created");
        }
    }
}
