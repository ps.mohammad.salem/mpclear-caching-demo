package com.progressoft.mpclear.cache.demo.persistence.dao;

import com.progressoft.mpclear.cache.demo.model.Customer;
import com.progressoft.mpclear.cache.demo.requests.AccountRequest;

public interface CustomerDao {
    void save(Customer customer);

    void update(AccountRequest account);

    Customer getCustomerByAlias(String alias);

    Customer getCustomerByMobile(String mobile);
}
