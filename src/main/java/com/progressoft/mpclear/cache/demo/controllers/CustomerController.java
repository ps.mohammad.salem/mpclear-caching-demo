package com.progressoft.mpclear.cache.demo.controllers;

import com.progressoft.mpclear.cache.demo.model.Customer;
import com.progressoft.mpclear.cache.demo.requests.AccountRequest;
import com.progressoft.mpclear.cache.demo.services.CustomerService;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@AllArgsConstructor
public class CustomerController {
    private final CustomerService service;

    @PutMapping(path = "/customer/account")
    public void update(@Valid @RequestBody AccountRequest accountRequest) {
        service.update(accountRequest);
    }

    @GetMapping(path = "/customer/byMobile/{mobile}")
    public Customer getByMobile(@PathVariable String mobile) {
        return service.getByMobile(mobile);
    }

    @GetMapping(path = "/customer/byAlias/{alias}")
    public Customer getByAlias(@PathVariable String alias) {
        return service.getByAlias(alias);
    }
}
