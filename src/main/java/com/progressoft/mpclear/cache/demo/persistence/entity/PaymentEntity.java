package com.progressoft.mpclear.cache.demo.persistence.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.Id;
import java.io.Serializable;
import java.math.BigDecimal;

@Data
@Entity
@NoArgsConstructor
@AllArgsConstructor
public class PaymentEntity implements Serializable {
    @Id
    private String id;
    private String debtorId;
    private String debtorName;
    private String debtorMobile;
    private String creditorId;
    private String creditorName;
    private String creditorMobile;
    private String type;
    private String session;
    private BigDecimal amount;
}
