package com.progressoft.mpclear.cache.demo.persistence.dao;

import com.progressoft.mpclear.cache.demo.model.Session;
import com.progressoft.mpclear.cache.demo.persistence.repository.JpaSessionRepository;
import com.progressoft.mpclear.cache.demo.persistence.entity.SessionEntity;
import com.progressoft.mpclear.cache.demo.mappers.SessionMapper;
import lombok.AllArgsConstructor;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component
@AllArgsConstructor
public class SessionDaoImpl implements SessionDao {
    private final SessionMapper sessionMapper;
    private final JpaSessionRepository jpaSessionRepository;

    @Override
    @CacheEvict(value = "sessions", allEntries = true)
    public void open(Session session) {
        SessionEntity sessionEntity = sessionMapper.toEntity(session);
        sessionEntity.setState("open");
        jpaSessionRepository.save(sessionEntity);
    }

    @Override
    @CacheEvict(value = "sessions", allEntries = true)
    public void close(String id) {
        SessionEntity sessionEntity = jpaSessionRepository.getOne(id);
        sessionEntity.setState("closed");
        jpaSessionRepository.save(sessionEntity);
    }

    @Override
    @Cacheable("sessions")
    public List<Session> list() {
        return jpaSessionRepository.findAllByState("open").stream()
                .map(sessionMapper::toModel)
                .collect(Collectors.toList());
    }
}
