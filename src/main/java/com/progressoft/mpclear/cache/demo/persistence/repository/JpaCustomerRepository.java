package com.progressoft.mpclear.cache.demo.persistence.repository;

import com.progressoft.mpclear.cache.demo.persistence.entity.CustomerEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface JpaCustomerRepository extends JpaRepository<CustomerEntity, String> {
}
