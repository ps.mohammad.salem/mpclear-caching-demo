package com.progressoft.mpclear.cache.demo.persistence.dao;

import com.progressoft.mpclear.cache.demo.mappers.PaymentMapper;
import com.progressoft.mpclear.cache.demo.model.Payment;
import com.progressoft.mpclear.cache.demo.persistence.repository.JpaPaymentRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Component;

@Component
@AllArgsConstructor
public class PaymentDaoImpl implements PaymentDao{
    private final PaymentMapper paymentMapper;
    private final JpaPaymentRepository jpaPaymentRepository;
    @Override
    public void persist(Payment payment) {
        jpaPaymentRepository.save(paymentMapper.toEntity(payment));
    }
}
