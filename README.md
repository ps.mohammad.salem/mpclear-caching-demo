# mpclear caching demo

1. showing how to use spring caching annotations
1. how configure external caching server
1. how to monitor caching server

## Design

![Design](design.png)

## How to run

1. pull application from git
1. open terminal on directory root and run **make all**
1. open another terminal on directory root and run **make post**

## Access Urls

1. app: localhost:9090
2. management center: localhost:8080
