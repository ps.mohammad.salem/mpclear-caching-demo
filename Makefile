build:
	mvn clean package
docker:
	docker rmi -f mpclear-cache-demo
	cp -r target/*.jar src/main/docker/application.jar
	docker build --no-cache -f src/main/docker/Dockerfile src/main/docker
docker-compose:
	docker-compose -f src/main/docker/docker-compose.yaml down
	docker-compose -f src/main/docker/docker-compose.yaml up
post:
	k6 run src/main/k6/post.js
all: build docker docker-compose